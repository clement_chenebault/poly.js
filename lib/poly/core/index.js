'use strict';

Object.defineProperty(exports, "__esModule", {
        value: true
});

var _Vector = require('./Vector');

var _Vector2 = _interopRequireDefault(_Vector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
        Vector: _Vector2.default
};
module.exports = exports['default'];
//# sourceMappingURL=index.js.map